package com.chromaway.aiponi;

import net.postchain.gtx.GTXModule;
import net.postchain.gtx.GTXModuleFactory;
import org.apache.commons.configuration2.Configuration;
import org.jetbrains.annotations.NotNull;


public class ModuleFactory implements GTXModuleFactory {
    @NotNull
    @Override
    public GTXModule makeModule(Configuration configuration) {
        return new Module();
    }
}
